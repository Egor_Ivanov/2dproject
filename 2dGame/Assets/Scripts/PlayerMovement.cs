﻿using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : CharacterMovement
{
    [SerializeField] private Animator animator;
    [SerializeField] private Transform graphics;
    [SerializeField] private float walkSpeed = 3f;
    [SerializeField] private float runSpeed = 6f;
    [SerializeField] private float jumpVel = 5f;
    [SerializeField] private float doubleJumpVel = 5f;
    [SerializeField] private float doubleJumpTimelimit = 0.25f;

    private Rigidbody2D rig;
    private bool isDubleJumped;
    private float jumpTimePassed;
    private bool isRunning;

    private void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        InputController.JumpAction += OnJumpAction;
        InputController.RunAction += OnRunAction;
    }

    private void Update()
    {
        if (IsGrounded())
        {
            var speed = rig.velocity.x;
            animator.SetFloat("Speed", Mathf.Abs(speed));
            animator.SetBool("Running", isRunning);
        }
        else
        {
            jumpTimePassed += Time.deltaTime;

            animator.SetFloat("Speed", 0f);
        }
        
        if (Mathf.Abs(rig.velocity.x) < 0.01f)
        {
            return;
        }

        var angle = rig.velocity.x > 0f ? 0f : 180f;
        graphics.localEulerAngles = new Vector3(0f, angle, 0f);
    }

    private void OnDestroy()
    {
        InputController.JumpAction -= OnJumpAction;
        InputController.RunAction -= OnRunAction;
    }

    public override void Move(Vector2 direction)
    {
        var velocity = rig.velocity;
        velocity.x = direction.x * (isRunning ? runSpeed : walkSpeed);
        rig.velocity = velocity;
    }

    public override void Stop(float timer)
    {
        throw new System.NotImplementedException();
    }

    public override void Jump(float force)
    {
        rig.velocity = new Vector2(0, force);
    }
    
    private void FixedUpdate()
    {
        if (IsFreezing)
        {
            Vector2 velocity = rig.velocity;
            velocity.x = 0f;
            rig.velocity = velocity;
            
            return;
        }

        var direction = new Vector2(InputController.HorizontalAxis, 0f);
        if (!IsGrounded())
        {
            direction *= 0.7f;
        }
        
        Move(direction);
    }

    private void OnJumpAction()
    {
        if (IsGrounded() && !IsFreezing)
        {
            isDubleJumped = false;
            jumpTimePassed = 0;
            animator.SetTrigger("Jump");
            Jump(jumpVel);
        }
        else
        {
            if (!isDubleJumped && jumpTimePassed < doubleJumpTimelimit)
            {
                Jump(doubleJumpVel);
                isDubleJumped = true;
            }
        }
    }

    private bool IsGrounded()
    {
        Vector2 point = transform.position;
        point.y -= 0.1f;
        RaycastHit2D hit = Physics2D.Raycast(point, Vector2.down, 0.2f);

        return hit.collider != null;
    }

    private void OnRunAction()
    {
        isRunning = !isRunning;
    }
}
