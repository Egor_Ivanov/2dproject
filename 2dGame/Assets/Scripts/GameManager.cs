﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public enum GameState
{
    MainMenu,
    Loading,
    Game,
    GamePause,
}

public class GameManager : MonoBehaviour
{
    public Interfaces.IPlayer Player;
    public IList<Interfaces.IEnemy> Enemies = new List<Interfaces.IEnemy>();
    
    public static Action<GameState> GameStateAction;

    private static GameState currentGameState;
    public static GameState GetGameState => currentGameState;
    
    void Start()
    {
        SetGameState(GameState.Game);
        print(Player);
        print($"Enemies count {Enemies.Count}");
    }

    public static void SetGameState(GameState newGameState)
    {
        currentGameState = newGameState;
        GameStateAction?.Invoke(newGameState);
    }

    [ContextMenu("Hide all characters")]
    public void HideAllCharacters()
    {
        var characters = FindCharacters();
        foreach (var character in characters)
        {
            character.SetActive(false);
        }
    }

    [ContextMenu("Show all characters")]
    public void ShowAllCharacters()
    {
        var characters = FindCharacters();
        foreach (var character in characters)
        {
            character.SetActive(true);
        }
    }

    private IReadOnlyList<GameObject> FindCharacters()
    {
        var ch = Resources.FindObjectsOfTypeAll<BaseEnemy>()
            .Select(e => e.gameObject).ToList();
        
        ch.AddRange(Resources.FindObjectsOfTypeAll<Player>().Select(x => x.gameObject).ToList());

        return ch;
    }
}
