﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarBonus : MonoBehaviour
{
    [SerializeField] private ParticleSystem _particleSystem;
    [SerializeField] private float _effectLifeTime;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    
    private void Start()
    {
        _particleSystem.Stop();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<Player>())
        {
            _spriteRenderer.enabled = false;
            _particleSystem.Play();
            Destroy(gameObject, _effectLifeTime);
        }
    }
}
