﻿using System;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public static float HorizontalAxis;
    public static event Action JumpAction;
    public static event Action<string> FireAction;
    public static event Action RunAction;

    void Start()
    {
        HorizontalAxis = 0f;
    }

    void Update()
    {
        HorizontalAxis = Input.GetAxis("Horizontal");

        if (Input.GetButtonDown("Jump"))
        {
            JumpAction.Invoke();
        }

        if (Input.GetKeyDown("v"))
        {
            RunAction.Invoke();
        }

        if (Input.GetButtonDown("Fire1"))
        {
            FireAction?.Invoke("Fire1");
        }
        
        if (Input.GetButtonDown("Fire2"))
        {
            FireAction?.Invoke("Fire2");
        }
    }

    private void OnDestroy()
    {
        HorizontalAxis = 0;
    }
}
