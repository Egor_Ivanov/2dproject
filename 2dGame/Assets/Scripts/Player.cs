﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Player : MonoBehaviour, Interfaces.IPlayer, Interfaces.IHitBox
{
    [SerializeField] private int health;
    [SerializeField] private Animator animator;
    
    private PlayerWeapon[] weapons;
    
    public void RegisterPlayer()
    {
        GameManager manager = FindObjectOfType<GameManager>();

        if (manager.Player == null)
        {
            manager.Player = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Awake()
    {
        RegisterPlayer();
        weapons = GetComponents<PlayerWeapon>();
        
        InputController.FireAction += Attack;
    }

    private void OnDestroy()
    {
        InputController.FireAction -= Attack;
    }

    private void Attack(string button)
    {
        foreach (var weapon in weapons)
        {
            if (weapon.ButtonName == button)
            {
                weapon.SetDamage();
            }
        }
    }

    public int Health
    {
        get => health;
        private set
        {
            health = value;
            if (health <= 0)
            {
                Die();
            }
        }
    }

    public void Hit(int damage)
    {
        Health -= damage;
    }
    
    public void Die()
    {
        print("Player died");
    }
}
