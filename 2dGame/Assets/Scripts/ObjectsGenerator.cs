﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsGenerator : MonoBehaviour
{
    [SerializeField] private GameObject[] prefabs;
    [SerializeField] private bool isRandom;
    [SerializeField] private float spawnInterval = 1f;

    private int currentPrefabIndex;
    
    private void Start()
    {
        StartCoroutine(CreateProcess());
    }

    private IEnumerator CreateProcess()
    {
        while (true)
        {
            yield return new WaitForSeconds(spawnInterval);

            if (isRandom)
            {
                currentPrefabIndex = Random.Range(0, prefabs.Length);
            }
                        
            var obj = Instantiate(prefabs[currentPrefabIndex]);
            obj.transform.position = transform.position;
            
            if (isRandom)
            {
                continue;
            }
            
            currentPrefabIndex++;
            if (currentPrefabIndex == prefabs.Length)
            {
                currentPrefabIndex = 0;
            }
        }
    }
}
