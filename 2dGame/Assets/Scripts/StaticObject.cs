﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticObject : MonoBehaviour, Interfaces.IHitBox
{
    [SerializeField] private LevelObjectData objectData;

    private Rigidbody2D rig;
    private int health = 1;

    public int Health
    {
        get => health;
        set
        {
            health = value;
            if (health <= 0)
            {
                Die();
            }
        }
    }

    private void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        Health = objectData.Health;
        if (rig)
        {
            rig.bodyType = objectData.Static ? RigidbodyType2D.Static : RigidbodyType2D.Dynamic;
        }
    }

    public void Hit(int damage)
    {
        Health -= damage;
    }

    public void Die()
    {
        print("Object died");
    }

    [ContextMenu("Rename")]
    private void Rename()
    {
        if (objectData != null)
        {
            gameObject.name = objectData.Name;
        }
    }
}
