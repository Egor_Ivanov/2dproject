﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

[System.Serializable]
public class GameData
{
    [System.Serializable]
    public class LevelData
    {
        public string LevelName;
        public int Difficulty;
    }
    
    public LevelData[] LevelsData;
}

public class GameDataEditor : EditorWindow
{
    public GameData GameData;

    private string projectGameDataPath = "/Data/GameData.json";

    [MenuItem("Tools/Game Data Editor")]
    private static void Init()
    {
        GetWindow(typeof(GameDataEditor)).Show();
    }

    private void OnGUI()
    {
        if (GameData != null)
        {
             var serializedObject = new SerializedObject(this);
             var serializedProperty = serializedObject.FindProperty("GameData");

            EditorGUILayout.PropertyField(serializedProperty, true);
            serializedObject.ApplyModifiedProperties();
            if (GUILayout.Button("Save Data"))
            {
                SaveGameData();
            }
        }
        
        if (GUILayout.Button("Load Data"))
        {
            LoadGameData();
        }
    }

    private void SaveGameData()
    {
        var dataAsJson = JsonUtility.ToJson(GameData);
        var filePath = $"{Application.dataPath}{projectGameDataPath}";
        File.WriteAllText(filePath, dataAsJson);

        AssetDatabase.Refresh();
    }

    private void LoadGameData()
    {
        var filePath = $"{Application.dataPath}{projectGameDataPath}";
        if (File.Exists(filePath))
        {
            var dataAsJson = File.ReadAllText(filePath);
            GameData = JsonUtility.FromJson<GameData>(dataAsJson);
        }
        else
        {
            GameData = new GameData();
        }
    }
}
