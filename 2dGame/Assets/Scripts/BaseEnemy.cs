﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using UnityEngine;

public enum EEnemyState
{
    Sleep,
    Wait,
    StartWalk,
    Walk,
    StartAttack,
    Attack,
    Retreat,
}

public class BaseEnemy : MonoBehaviour, Interfaces.IEnemy, Interfaces.IHitBox
{
    [SerializeField] private Animator animator;
    [SerializeField] private int health;
    [SerializeField] private Transform checkGroundPoint;
    [SerializeField] private Transform checkAttackPoint;
    [SerializeField] private Transform graphics;

    private GameManager _gameManager;
    private EEnemyState _currentState = EEnemyState.Sleep;
    private EEnemyState _nextState;
    private float _wakeUpTimer;
    private float _waitTimer;
    private float _attackTimer;
    private float _currentDirection = 1f;

    private void Awake()
    {
        RegisterEnemy();
        _wakeUpTimer = Time.time + 1f;
    }

    private void Update()
    {
        switch (_currentState)
        {
            case EEnemyState.Sleep:
                Sleep();
                break;
            case EEnemyState.Wait:
                Wait();
                break;
            case EEnemyState.StartWalk:
                animator.SetInteger("Walking", 1);
                _currentState = EEnemyState.Walk;
                break;
            case EEnemyState.Walk:
                Walk();
                break;
            case EEnemyState.StartAttack:
                animator.SetTrigger("Attack");
                ((Interfaces.IHitBox) _gameManager.Player).Hit(1);
                _currentState = EEnemyState.Attack;
                break;
            case EEnemyState.Attack:
                Attack();
                break;
            case EEnemyState.Retreat:
                Retreat();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public void RegisterEnemy()
    {
        _gameManager = FindObjectOfType<GameManager>();
        _gameManager.Enemies.Add(this);
    }
    
    public int Health
    {
        get => health;
        private set
        {
            health = value;
            if (health <= 0)
            {
                Die();
            }
        }
    }

    public void Hit(int damage)
    {
        Health -= damage;
        if (Health > 0)
        {
            _currentState = EEnemyState.Retreat;
        }
    }
    
    public void Die()
    {
        animator.SetTrigger("Die");
        Destroy(this);
        Destroy(gameObject, 1f);
    }

    private void Sleep()
    {
        if (Time.time >= _wakeUpTimer)
        {
            WakeUp();
        }
    }

    private void WakeUp()
    {
        var playerPosition = ((MonoBehaviour) _gameManager.Player).transform.position;
        if (Vector3.Distance(transform.position, playerPosition) > 20f)
        {
            StartSleeping();
            return;
        }

        _currentState = EEnemyState.Wait;
        _nextState = EEnemyState.StartWalk;
        _waitTimer = Time.time + 0.1f;
    }

    private void StartSleeping(float sleepTime = 1f)
    {
        _wakeUpTimer = Time.time + sleepTime;
        _currentState = EEnemyState.Sleep;
    }

    private void Wait()
    {
        if (Time.time >= _waitTimer)
        {
            _currentState = _nextState;
        }
    }

    private void Walk()
    {
        transform.Translate(transform.right * Time.deltaTime * _currentDirection);
        RaycastHit2D hit = Physics2D.Raycast(checkGroundPoint.position, Vector2.down, 0.3f);

        if (hit.collider == null)
        {
            _currentDirection *= -1;
            float angle = _currentDirection > 0 ? 0f : 180f;
            graphics.localEulerAngles = new Vector3(0f, angle, 0f);
            _currentState = EEnemyState.Wait;
            _waitTimer = Time.time + 0.3f;
            _nextState = EEnemyState.StartWalk;
            
            animator.SetInteger("Walking", 0);
            return;
        }
        
        hit = Physics2D.Raycast(checkGroundPoint.position, checkAttackPoint.right, 0.3f);
        if (hit.collider != null)
        {
            var player = hit.collider.GetComponent<Player>();
            if (player)
            {
                _currentState = EEnemyState.StartAttack;
            }
        }
    }

    private void Attack()
    {
        if (Time.time < _attackTimer)
        {
            return;
        }

        _currentState = EEnemyState.Wait;
        _nextState = EEnemyState.StartWalk;
        _waitTimer = Time.time + 0.2f;
    }
    
    private void Retreat()
    {
        RaycastHit2D hit = Physics2D.Raycast(checkGroundPoint.position, checkAttackPoint.right, 0.5f);
        var player = hit.collider.GetComponent<Player>();
        if (player)
        {
            _currentDirection *= -1;
            float angle = _currentDirection > 0 ? 0f : 180f;
            graphics.localEulerAngles = new Vector3(0f, angle, 0f);
            _nextState = EEnemyState.StartWalk;
            animator.SetInteger("Walking", 0);
        }
    }
}
