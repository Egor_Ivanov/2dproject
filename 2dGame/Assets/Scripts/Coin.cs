﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private float movementDistance = 1.5f;

    private Coroutine _moveUpCoroutine;

    private void OnTriggerEnter2D(Collider2D other)
    {
        var playerObj = other.GetComponent<Player>();
        if (_moveUpCoroutine == null && playerObj)
        {
            _moveUpCoroutine = StartCoroutine(MoveUp());
        }
    }

    private IEnumerator MoveUp()
    {
        animator.SetTrigger("Rotate");

        var distance = 0f;
        while (distance < movementDistance)
        {
            var shift = movementDistance * Time.deltaTime;
            transform.Translate(Vector3.up * shift);
            distance += shift;

            yield return null;
        }

        Destroy(gameObject);
    }
}
