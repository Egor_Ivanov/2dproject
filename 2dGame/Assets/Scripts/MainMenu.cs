﻿using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private GameObject mainPanel;
    [SerializeField] private GameObject settingsPanel;

    [SerializeField] private Button loadLevelButton;
    [SerializeField] private Button settingButton;
    [SerializeField] private Button closeButton;

    [SerializeField] private InputField nicknameInput;
    
    void Start()
    {
        GameManager.SetGameState(GameState.MainMenu);
        mainPanel.SetActive(true);
        settingsPanel.SetActive(false);
        
        loadLevelButton.onClick.AddListener(OnLoadLevel);
    }

    private void OnLoadLevel()
    {
        SceneLoader.LoadLevel("Platformer");
    }

    public void LoadLevel(string level)
    {
        SceneLoader.LoadLevel(level);
    }

}
