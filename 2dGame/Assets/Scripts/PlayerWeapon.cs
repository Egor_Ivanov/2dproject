﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeapon : MonoBehaviour, Interfaces.IDamager
{
    [SerializeField] private WeaponData weaponData;
    [SerializeField] private Transform attackPoint;
    [SerializeField] private string buttonName = "Fire1";
    
    public int Damage => weaponData.WeaponDamage;
    public string ButtonName => buttonName;

    private float lastAttackTime;
    
    public void SetDamage()
    {
        if (Time.time - lastAttackTime < weaponData.FireRate)
        {
            return;
        }

        lastAttackTime = Time.time;
        
        var target = GetTarget();
        target?.Hit(weaponData.WeaponDamage);
    }

    private Interfaces.IHitBox GetTarget()
    {
        Interfaces.IHitBox target = null;
        RaycastHit2D hit = Physics2D.Raycast(attackPoint.position, attackPoint.right, weaponData.WeaponRange);

        if (hit.collider != null)
        {
            target = hit.transform.gameObject.GetComponent<Interfaces.IHitBox>();
        }

        return target;
    }
}
